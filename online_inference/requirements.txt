scikit-learn==0.22.2.post1
pandas==0.25.1
numpy==1.19.4
fastapi==0.65.1
uvicorn==0.13.4
pytest==5.4.3
click==7.0